from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json


class  LocationVOEncoder(ModelEncoder):
	model = LocationVO
	properties = [
		"id",
		"closet_name",
		"section_number",
		"shelf_number",
	]
class HatListEncoder(ModelEncoder):
	model = Hat
	properties = [
		"id",
		"style_name",
	]

class HatDetailEncoder(ModelEncoder):
	model = Hat
	properties = [
		"style_name",
		"fabric",
		"id",
		"color",
		"picture_url",
		"location"
	]
	encoders = {"location": LocationVOEncoder()}

@require_http_methods(["GET", "POST"])
# def api_list_hats(request):
def api_list_hats(request, location_vo_id=None):
	if request.method == "GET":
		hats = Hat.objects.all()
		return JsonResponse(
			{"hats": hats},
			encoder=HatDetailEncoder,
		)
	else:
		content = json.loads(request.body)

		try:
			location_id = content["location"]
			location_href = f"/api/locations/{location_id}/"
			# hat_location = LocationVO.objects.get(id=location_id)
			print(len(LocationVO.objects.all()))
			hat_location = LocationVO.objects.get(import_href=location_href)

			content["location"] = hat_location
		except LocationVO.DoesNotExist:
			return JsonResponse(
				{"message": "location ID does not exist"},
				status=400,
			)

		hat = Hat.objects.create(**content)
		return JsonResponse(
			hat,
			encoder=HatDetailEncoder,
			safe=False,
		)

@require_http_methods(['DELETE', "GET", "PUT"])
def api_show_hat(request, pk):
	if request.method == "GET":
		hat = Hat.objects.get(id=pk)
		return JsonResponse(
			hat,
			encoder=HatDetailEncoder,
			safe=False,
		)

	elif request.method == "DELETE":
		count, _ = Hat.objects.filter(id=pk).delete()
		return JsonResponse({"deleted": count > 0})

	else:
		content = json.loads(request.body)
		Hat.objects.filter(id=pk).update(**content)
		hat = Hat.objects.get(id=pk)
		return JsonResponse(
			hat,
			encoder=HatDetailEncoder,
			safe=False,
		)


# @require_http_methods(["GET", "POST"])
# def list_api_locations(request):

#     if request.method == "GET":
#         locations = LocationVO.objects.all()
#         return JsonResponse(
#             {"locations": locations},
#             encoder=LocationVOEncoder,
#         )
#     else:
#         content = json.loads(request.body)
#         location = LocationVO.objects.create(**content)
#         return JsonResponse(
#             location,
#             encoder=LocationVOEncoder,
#             safe=False,
#         )

# @require_http_methods(["DELETE", "GET", "PUT"])
# def show_api_location(request, pk):

#     if request.method == "GET":
#         try:
#             location = LocationVO.objects.get(id=pk)
#             return JsonResponse(
#                 location,
#                 encoder=LocationVOEncoder,
#                 safe=False
#             )
#         except LocationVO.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
#     elif request.method == "DELETE":
#         try:
#             location = LocationVO.objects.get(id=pk)
#             location.delete()
#             return JsonResponse(
#                 location,
#                 encoder=LocationVOEncoder,
#                 safe=False,
#             )
#         except LocationVO.DoesNotExist:
#             return JsonResponse({"message": "Does not exist"})
#     else: # PUT
#         try:
#             content = json.loads(request.body)
#             location = LocationVO.objects.get(id=pk)

#             props = ["closet_name", "shelf_number", "section_number"]
#             for prop in props:
#                 if prop in content:
#                     setattr(location, prop, content[prop])
#             location.save()
#             return JsonResponse(
#                 location,
#                 encoder=LocationVOEncoder,
#                 safe=False,
#             )
#         except LocationVO.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response




# @require_http_methods(["GET", "POST"])
# def api_list_locations(request):
#     if request.method == "GET":
#         locations = Location.objects.all()
#         return JsonResponse(
#             {"locations": locations},
#             encoder=LocationListEncoder,
#         )
#     else:
#         content = json.loads(request.body)

#         try:
#             location = Location.objects.get(location=content["location"])
#             content["location"] = location
#         except Location.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid location"},
#                 status=400,
#             )

#         # photo = get_photo(content["city"], content["state"].abbreviation)
#         # content.update(photo)
#         # location = Location.objects.create(**content)
#         return JsonResponse(
#             location,
#             encoder=LocationDetailEncoder,
#             safe=False,
#         )

# @require_http_methods(["DELETE", "GET", "POST"])
# def api_show_location(request, pk):
# 	if request.method == "GET":
# 		location = Location.objects.get(id=pk)
# 		return JsonResponse(
# 			location,
# 			encoder=LocationDetailEncoder,
# 			safe=False
# 		)
# 	if request.method == "DELETE":
# 		count, _ = Location.objects.filter(id=pk).delete()
# 		return JsonResponse({"deleted": count > 0})
# 	else:
# 		content = json.loads(request.body)
# 		try:
# 			if "location" in content:
# 				location = Location.objects.get(location=content["location"])
# 				content["location"]=location
# 		except Location.DoesNotExist:
# 			return JsonResponse(
# 				{"message": "Invalid location"},
# 				status=400,
# 			)
# 		Location.objects.filter(id=pk).update(**content)
# 		location = Location.objects.get(id=pk)
# 		return JsonResponse(
# 			location,
# 			encoder=LocationDetailEncoder,
# 			safe=False,
# 		)
