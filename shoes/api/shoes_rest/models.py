from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=156)
    bin_number = models.PositiveSmallIntegerField(null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True, blank=True)
    import_href = models.CharField(max_length=200, null=True, blank=True, unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=126)
    shoe_name = models.CharField(max_length=126)
    shoe_color = models.CharField(max_length=56)
    shoe_photo_url = models.URLField(blank=True, null=True)
    bin = models.ForeignKey(
        BinVO, 
        related_name="shoe", 
        null=True, blank=False, 
        on_delete=models.CASCADE)

    def __str__(self):
        return self.shoe_name
    
    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})


	def get_api_url(self):
		return reverse("api_shoe", kwargs={"pk": self.pk})