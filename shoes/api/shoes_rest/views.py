from django.shortcuts import render
from common.json import ModelEncoder
from .models import BinVO, Shoe
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", 
                  "bin_number", 
                  "bin_size", 
                  "import_href",]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        # "manufacturer",
        "shoe_name",
        # "shoe_color",
        # "shoe_photo_url",
        # "bin",
    ]

    encoders = {"bin": BinVOEncoder()}

class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        # "manufacturer",
        "shoe_name",
        # "shoe_color",
        # "shoe_photo_url",
        # "bin",
    ]
    encoders = {"Bin: binVoEncoder()"}
    
@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {'shoes': shoes},
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_id = content["bin"]
            bin_href = f"/api/bins/{bin_id}/"
        # hat_location = LocationVO.objects.get(id=location_id)
            print(len(BinVO.objects.all()))
            shoe_bin = BinVO.objects.get(import_href=bin_href)
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
                shoes,
                encoder=ShoesDetailEncoder,
                safe=False,
        )

@require_http_methods(['DELETE', "GET", "PUT"])
def api_showshoe(request, pk):
    if request.method == "GET":
        hat = Shoe.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count,  = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        shoes = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

