import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));




function HatList (props) {
    const [hats, setHats] = useState([]);
    const getHats = async () => {
        const hatsUrl = "http://localhost:8090/api/hats/";
        const response = await fetch(hatsUrl);

        if (response.ok) {
            const ListHats = await response.json();
            setHats(HatList.hats);
        }
    }
    useEffect(() => {getHats() }, []);

    const deleteHat = (id) => async () => {
        try {
            const url = `http://localhost:8090/api/hats/${id}/`;
            const deleteResponse = await fetch(url, { method: "delete" });

            if (deleteResponse.ok) {
                const reloadResponse = await fetch("http://localhost:8090/api/hats/");
                const newHats = await reloadResponse.json();
                setHats(newHats.hats);
            }
        }
        catch (e) { }
    }
    return (
        <div>
        <h3>Hat List</h3>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Delete Hat</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.closet_name }</td>
                            <td>
                                <button onClick={() => deleteHat(hat.id)}
                                >Delete Hat</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default HatList