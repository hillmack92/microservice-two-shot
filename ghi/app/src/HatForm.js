// import React from 'react';

class HatForm extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			styleName: '',
			fabric: '',
			color: '',
			locations: []
		};
		this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
		this.handleFabricChange = this.handleFabricChange.bind(this);
		this.handleColorChange = this.handleColorChange.bind(this);
		this.handleLocationChange = this.handleStyleNameChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	async handleSubmit(event) {
		event.preventDefault();
		const data = {...this.state};
		delete data.styleName;

		const locationUrl = 'http://localhost:8000/api/locations/';
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		const response = await fetch(locationUrl, fetchConfig);
		if(response.ok) {
			const newLocation = await response.json();
			console.log(newLocation);

			const cleared = {
				styleName: '',
				fabric: '',
				color: '',
				location: '',
			};
			this.setState(cleared);
		}
	}

	handleStyleNameChange(event) {
		const value = event.target.value;
		this.setState({styleName: value})
	}

	handleFabricChange(event) {
		const value = event.target.value;
		this.setState({fabric: value})
	}

	handleColorChange(event) {
		const value = event.target.value;
		this.setState({color: value})
	}

	handleLocationChange(event) {
		const value = event.target.value;
		this.setState({location: value})
	}

	handleFabricChange(event) {
		const value = event.target.value;
		this.setState({fabric: value})
	}

	handleColorChange(event) {
		const value = event.target.value;
		this.setState({color: value})
	}

	handleLocationChange(event) {
		const value = event.target.value;
		this.setState({location: value})
	}


	async componentDidMount() {
		const url = 'http://localhost:8000/api/location';

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			this.setState({location: data.location})
		}
	}

	render() {
		return (
			<div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Place a new hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStyleNameChange} value={this.state.styleName} placeholder="Style Name" required type="text" name="style_name" id="Style Name" className="form-control"/>
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="number" name="fabric" id="fabric" className="form-control"/>
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="city" id="city" className="form-control"/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.location.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
		)
	}
}

// export default HatForm;