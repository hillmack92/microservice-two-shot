import React, { Component } from 'react'

export default class ShoesForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            shoeName: '',
            shoeColor: '',
            shoePhotoUrl: '',
            bin: '',
            bins: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleShoeColorChange = this.handleShoeColorChange.bind(this);
        this.handleShoePhotoUrlChange = this.handleShoePhotoUrlChange.bind(this);
        this.handleShoeNameChange = this.handleShoeNameChange.bind(this);
    };

	async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        // data.shoes_color = data.shoeColor;
        data.shoe_name = data.shoeName;
        // data.shoe_photo_url = data.shoePhotoUrl;
        delete data.shoeName;
        delete data.shoePhotoUrl;
        delete data.shoeColor;
        delete data.bins;
        const shoesURL = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

		const response = await fetch(shoesURL, fetchConfig);
		if(response.ok) {
			const newshoe = await response.json();
			console.log(newshoe);

			const cleared = {
				manufacturer: '',
				shoeName: '',
				shoeColor: '',
				shoePhotoUrl: '',
				bin: '',
			};
			this.setState(cleared);
		}
	}

	handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }

    handleShoeNameChange(event) {
        const value = event.target.value;
        this.setState({ shoeName: value })
    }

    handleShoeColorChange(event) {
        const value = event.target.value;
        this.setState({ shoeColor: value })
    }
    
    handleShoePhotoUrlChange(event) {
        const value = event.target.value;
        this.setState({ shoePhotoUrl: value})
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({ bin: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins })
        }
    }

    render() {
        return (
            <>
                <div className="my-5 container" >
                    <div className="row">
                        <div className="offset-3 col-6">
                            <div className="shadow p-4 mt-4">
                                <h1>Create a new shoe!</h1>
                                <form onSubmit={this.handleSubmit} id="create-shoe-form">
                                    <div className="form-floating mb-3">
                                        <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                        <label htmlFor="manufacturer">Manufacturer</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input onChange={this.handleShoeNameChange} value={this.state.shoeName} placeholder="Shoe Name" required type="text" name="shoeName" id="shoeName" className="form-control" />
                                        <label htmlFor="shoeName">Shoe Name</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input onChange={this.handleShoeColorChange} value={this.state.shoeColor} placeholder="Shoe Color" required type="text" name="shoeColor" id="shoeColor" className="form-control" />
                                        <label htmlFor="shoeColor">Shoe Color</label>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input onChange={this.handleShoePhotoUrlChange} value={this.state.shoePhotoUrl} placeholder="Photo URL" required type="text" name="shoePhotoUrl" id="shoePhotoUrl" className="form-control" />
                                        <label htmlFor="shoePhotoUrl">Photo</label>
                                    </div>
                                    <div className="mb-3">
                                        <select onChange={this.handleBinChange} multiple={false} value={this.state.bin} required name="bins" id="bins" className="form-select">
                                            <option value="">Choose a bin!</option>
                                            {this.state.bins.map(bin => {
                                                return (
                                                    <option key={bin.id} value={bin.href}>
                                                        {bin.closet_name}
                                                    </option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    <button className="btn btn-outline-dark">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
