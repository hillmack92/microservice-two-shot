import React from 'react'

export default function ShoesList({ shoes }) {
    const deleteShoe = async (id) => {
        fetch(`http://localhost:8080/api/shoes/${id}`, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => {
            window.location.reload();
        })
    }

    return (
        <>
            <table className="table table-hover table-dark table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Shoe Name</th>
                        <th>Shoe Color</th>
                        <th>Shoe Photo</th>
                        <th>No more have shoe?</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.bin.id}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.shoe_name}</td>
                                <td>{shoe.shoe_color}</td>
                                <td><img src={shoe.shoe_photo_url} alt="" width="20%" height="20%" /></td>
                                <td><button onClick={() => deleteShoe(shoe.id)} type="button" className="btn btn-outline-light">Delete me!</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}